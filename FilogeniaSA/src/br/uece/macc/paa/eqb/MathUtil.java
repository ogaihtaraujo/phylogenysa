/**
 * 
 */
package br.uece.macc.paa.eqb;

/**
 * @author thiago
 * 
 */
public class MathUtil {

	/**
	 * 
	 */
	public MathUtil() {
		// TODO Auto-generated constructor stub
	}

	public static int randomWithRange(int min, int max) {
		int range = Math.abs(max - min) + 1;
		return (int) (Math.random() * range) + (min <= max ? min : max);
	}

}
