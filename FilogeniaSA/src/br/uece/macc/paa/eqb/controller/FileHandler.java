/**
 * 
 */
package br.uece.macc.paa.eqb.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.uece.macc.paa.eqb.model.Instance;

/**
 * @author thiago
 * 
 */
public class FileHandler {

	private String inputFolder = "input";
	private String outputFolder = "output";

	/**
	 * 
	 */
	public FileHandler() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param inputFolder
	 * @param outputFolder
	 */
	public FileHandler(String inputFolder, String outputFolder) {
		super();
		this.inputFolder = inputFolder;
		this.outputFolder = outputFolder;
	}

	/**
	 * @return the inputFolder
	 */
	public String getInputFolder() {
		return inputFolder;
	}

	/**
	 * @return the outputFolder
	 */
	public String getOutputFolder() {
		return outputFolder;
	}

	/**
	 * @param inputFolder
	 *            the inputFolder to set
	 */
	public void setInputFolder(String inputFolder) {
		this.inputFolder = inputFolder;
	}

	/**
	 * @param outputFolder
	 *            the outputFolder to set
	 */
	public void setOutputFolder(String outputFolder) {
		this.outputFolder = outputFolder;
	}

	public List<Instance> readInstancesFromFile() {
		File arquivos[];
		File diretorio = new File(inputFolder);
		arquivos = diretorio.listFiles();
		List<Instance> instances = new ArrayList<Instance>();
		for (int i = 0; i < arquivos.length; i++) {
			String name = arquivos[i].getName().replaceAll("[.txt]", "");
			try {
				Scanner scanner = new Scanner(new FileReader(arquivos[i]));
				Scanner header = new Scanner(scanner.nextLine());

				int numberOfSpecies = header.nextInt();
				int numberOfCharacter = header.nextInt();
				int solution = 0;
				if (header.hasNext()) {
					String line = header.nextLine();
					solution = Integer.parseInt(line.replaceAll("[\\sa-zA-Z=]",
							""));
				}
				header.close();

				Instance instance = new Instance(name, numberOfSpecies,
						numberOfCharacter, solution);
				int j = 0;
				while (scanner.hasNext()) {
					Scanner line = new Scanner(scanner.nextLine());

					instance.getSpecies()[j] = line.next();
					String characteres = line.next().replaceAll("[\\s]", "");
					for (int w = 0; w < numberOfCharacter; w++) {
						instance.getMatrix()[j][w] = Integer
								.parseInt(characteres.substring(w, w + 1));
					}
					j++;
					line.close();
				}
				scanner.close();
				instances.add(instance);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return instances;
	}

	public static void main(String[] args) {
		FileHandler handler = new FileHandler("input", "output");
		List<Instance> instances = handler.readInstancesFromFile();
		for (Instance instance : instances) {
			System.out.println(instance.getName());

		}
	}

}
