package br.uece.macc.paa.eqb.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.uece.macc.paa.eqb.MathUtil;
import br.uece.macc.paa.eqb.model.Instance;

public class Phylogeny {

	public Phylogeny() {
		super();
	}

	/**
	 * Checks whether a tree configuration is viable
	 * 
	 * @param numberOfSpecies
	 * @param numberOfNodes
	 * @param tree
	 * @return
	 */
	public boolean isViable(int numberOfSpecies, int numberOfNodes, int[] tree) {
		boolean viable = true;
		int[] aux = new int[numberOfNodes + 1];
		for (int i = numberOfNodes - 1; i >= numberOfSpecies; i--) {
			if (tree[i] <= i) {
				return false;
			}
		}
		for (int i = 0; i < numberOfNodes; i++) {
			aux[tree[i]] = aux[tree[i]] + 1;
		}
		for (int i = numberOfSpecies; i < numberOfNodes; i++) {
			if (aux[i] != 2) {
				return false;
			}
		}
		return viable;
	}

	/**
	 * Calculates partial cost (an internal node) by the criterion of parsimony.
	 * 
	 * @param numberOfCharacter
	 * @param leftChild
	 * @param rightChild
	 * @return a map containing the cost and the parent of the node
	 */
	public HashMap<String, int[]> costNode(int numberOfCharacter,
			int[] leftChild, int[] rightChild) {
		int cost = 0;
		int father[] = new int[numberOfCharacter];

		HashMap<String, int[]> map = new HashMap<String, int[]>(2);
		for (int i = 0; i < numberOfCharacter; i++) {
			if (leftChild[i] + rightChild[i] == 1) {
				father[i] = 2;
				cost++;
			} else if (leftChild[i] < rightChild[i]) {
				father[i] = leftChild[i];
			} else {
				father[i] = rightChild[i];
			}
		}
		map.put("cost", new int[] { cost });
		map.put("father", father);
		return map;
	}

	/**
	 * Estimated total cost of a tree represented by z the criterion of
	 * parsimony. If the upper limit is reached the process is stopped returning
	 * cost = -1. Configuring Tree has the structure of vector and BigMatrix is
	 * a matrix of order (2m-1) x n with lines 1 to m equal to the input array
	 * of Matrix instance.
	 * 
	 * @param numberOfSpecies
	 * @param numberOfCharacter
	 * @param tree
	 * @param upperBound
	 * @param BigMatrix
	 * @return
	 */

	public int cost(int numberOfSpecies, int numberOfCharacter, int[] tree,
			int[][] BigMatrix) {
		int cost = 0;
		int numberOfNodes = 2 * numberOfSpecies - 2;
		// starts to go all nodes.
		for (int i = numberOfSpecies; i < numberOfNodes + 1; i++) {
			int leftIndex = -1;
			int rightIndex = -1;
			// Makes a local search in vector "tree" by the indexes of the
			// children of the right and left.
			for (int j = 0; j < tree.length; j++) {
				if (tree[j] == i) {
					if (leftIndex < 0) {
						leftIndex = j;
					} else {
						rightIndex = j;
					}
				}
			}
			int[] leftChild = new int[numberOfCharacter];
			int[] rightChild = new int[numberOfCharacter];
			// Creates the vectors of the children from the matrix.
			for (int w = 0; w < numberOfCharacter; w++) {
				leftChild[w] = BigMatrix[leftIndex][w];
				rightChild[w] = BigMatrix[rightIndex][w];
			}

			HashMap<String, int[]> map = costNode(numberOfCharacter, leftChild,
					rightChild);
			int[] father = map.get("father");
			cost += map.get("cost")[0];
			// insert the father into matrix
			for (int w = 0; w < numberOfCharacter; w++) {
				BigMatrix[i][w] = father[w];
			}
			/*
			 * if (cost >= upperBound) { return -1; }
			 */
		}
		return cost;
	}

	public int costMin(int numberOfSpecies, int numberOfCharacter, int[] tree,
			int[][] BigMatrix, int upperBound) {
		int cost = 0;
		int numberOfNodes = 2 * numberOfSpecies - 2;
		// starts to go all nodes.
		for (int i = numberOfSpecies; i < numberOfNodes + 1; i++) {
			int leftIndex = -1;
			int rightIndex = -1;
			// Makes a local search in vector "tree" by the indexes of the
			// children of the right and left.
			for (int j = 0; j < tree.length; j++) {
				if (tree[j] == i) {
					if (leftIndex < 0) {
						leftIndex = j;
					} else {
						rightIndex = j;
					}
				}
			}
			int[] leftChild = new int[numberOfCharacter];
			int[] rightChild = new int[numberOfCharacter];
			// Creates the vectors of the children from the matrix.
			for (int w = 0; w < numberOfCharacter; w++) {
				leftChild[w] = BigMatrix[leftIndex][w];
				rightChild[w] = BigMatrix[rightIndex][w];
			}

			HashMap<String, int[]> map = costNode(numberOfCharacter, leftChild,
					rightChild);
			int[] father = map.get("father");
			cost += map.get("cost")[0];
			// insert the father into matrix
			for (int w = 0; w < numberOfCharacter; w++) {
				BigMatrix[i][w] = father[w];
			}

			if (cost >= upperBound) {
				return -1;
			}

		}
		return cost;
	}

	public List<int[]> generateRandomInitialPopulation(Instance instance,
			int size) {
		List<int[]> initialPopulation = new ArrayList<int[]>();
		// TODO
		return initialPopulation;
	}

	public List<int[]> generateSequencialInitialPopulation(Instance instance,
			int size) {
		List<int[]> initialPopulation = new ArrayList<int[]>();
		// TODO
		return initialPopulation;
	}

	/**
	 * Generates a random configuration of a phylogenetic tree.
	 * 
	 * @param numberOfSpecies
	 * @return a tree
	 */
	public int[] generateRandomViableTree(int numberOfSpecies) {
		boolean valid = false;
		int numberOfNodes = (2 * numberOfSpecies) - 1;
		int[] tree = null;
		// int attempt = 0;
		while (!valid) {
			// ++attempt;
			tree = new int[numberOfNodes];
			int[] aux = new int[numberOfNodes + 1];
			for (int i = numberOfNodes - 1; i >= numberOfSpecies; i--) {
				int k = 0;
				// aumenta a probabilidade em gerar uma árvore válida reduzindo
				// o tempo do algoritmo
				while (k <= i) {
					k = MathUtil.randomWithRange(i, numberOfNodes);
				}
				if (aux[k] < 2 && k > i) {
					if (k == numberOfNodes) {
						aux[k] = 2;
					} else {
						aux[k] = aux[k] + 1;
					}
					tree[i] = k;
				}
			}
			for (int i = 0; i < numberOfSpecies; i++) {
				int k = MathUtil
						.randomWithRange(numberOfSpecies, numberOfNodes);
				if (aux[k] < 2) {
					aux[k]++;
					tree[i] = k;
				}
			}
			for (int i = (tree.length - 1); i >= 0; i--) {
				if (tree[i] == 0) {
					for (int j = aux.length - 1; j >= numberOfSpecies; j--) {
						if (aux[j] < 2) {
							if (j == numberOfNodes && aux[j] < 1) {
								aux[j] = 2;
								tree[i] = j;
							} else {
								aux[j]++;
								tree[i] = j;
							}
							break;
						}
					}
				}
			}
			valid = isViable(numberOfSpecies, numberOfNodes, tree);
		}
		// System.out.println(attempt);
		return tree;
	}

	/**
	 * Generate a population of phylogenetic trees.
	 * 
	 * @param size
	 * @param numberOfSpecies
	 * @return a list of trees
	 */
	public List<int[]> generatePopulation(int size, int numberOfSpecies) {
		List<int[]> population = new ArrayList<int[]>();
		for (int i = 0; i < size; i++) {
			int[] tree = generateRandomViableTree(numberOfSpecies);
			population.add(tree);
		}
		return population;
	}

	public int[] permute(int[] tree, int position1, int position2) {
		int[] permutedTree = tree;
		int aux = permutedTree[position2];
		permutedTree[position2] = permutedTree[position1];
		permutedTree[position1] = aux;

		return permutedTree;
	}

	public int[] permute(int[] tree, int position1, int position2, int position3) {
		int[] permutedTree = tree.clone();
		int aux = permutedTree[position3];
		permutedTree[position3] = permutedTree[position2];
		permutedTree[position2] = permutedTree[position1];
		permutedTree[position1] = aux;
		return permutedTree;
	}

	public int[] permute(int numberOfSpecies, int numberOfNodes, int[] tree) {
		do {
			int position1 = MathUtil.randomWithRange(0, numberOfSpecies - 1);
			int max = numberOfNodes - 1;
			int position2 = MathUtil.randomWithRange(numberOfSpecies, max);
			while (position2 >= numberOfNodes) {
				position2 = MathUtil.randomWithRange(numberOfSpecies, max);
			}
			while (tree[position1] == tree[position2]
					&& tree[position1] < position2) {
				position2 = MathUtil.randomWithRange(numberOfSpecies, max);
			}
			tree = permute(tree, position1, position2);
		} while (isViable(numberOfSpecies, numberOfNodes, tree));
		return tree;
	}

	public int[] permute3(int numberOfSpecies, int numberOfNodes, int[] tree) {
		do {
			int position1 = MathUtil.randomWithRange(0, numberOfSpecies - 1);
			int position2 = MathUtil.randomWithRange(numberOfSpecies,
					numberOfNodes - 1);
			int position3 = MathUtil.randomWithRange(0, numberOfNodes - 1);
			int configuration = MathUtil.randomWithRange(1, 6);
			while (position3 != position1 && position3 != position2) {
				position3 = MathUtil.randomWithRange(0, numberOfNodes - 1);
			}
			switch (configuration) {
			case 1:
				tree = permute(tree, position1, position2, position3);
				break;
			case 2:
				tree = permute(tree, position1, position3, position2);
				break;
			case 3:
				tree = permute(tree, position2, position3, position1);
				break;
			case 4:
				tree = permute(tree, position2, position1, position3);
				break;
			case 5:
				tree = permute(tree, position3, position1, position2);
				break;
			case 6:
				tree = permute(tree, position3, position2, position1);
				break;
			default:
				break;
			}
		} while (isViable(numberOfSpecies, numberOfNodes, tree));
		return tree;
	}

	public int[] permute3(int[] tree, int configuration, int position1,
			int position2, int position3) {
		int permutedTree[] = tree.clone();
		switch (configuration) {
		case 1:
			permutedTree = permute(permutedTree, position1, position3,
					position2);
			break;
		case 2:
			permutedTree = permute(permutedTree, position2, position3,
					position1);
			break;
		case 3:
			permutedTree = permute(permutedTree, position2, position1,
					position3);
			break;
		case 4:
			permutedTree = permute(permutedTree, position3, position1,
					position2);
			break;
		case 5:
			permutedTree = permute(permutedTree, position3, position2,
					position1);
			break;
		default:
			permutedTree = permute(permutedTree, position1, position2,
					position3);
			break;
		}
		return permutedTree;
	}
}
