/**
 * 
 */
package br.uece.macc.paa.eqb.controller;

import java.util.HashMap;
import java.util.List;

import br.uece.macc.paa.eqb.MathUtil;
import br.uece.macc.paa.eqb.model.Instance;
import br.uece.macc.paa.eqb.model.OutputSolution;

/**
 * @author thiago araujo
 * 
 */
public class SimulatedAnnealing {

	private double temperature = 2000;
	private double coolingRate = 0.99;
	private Instance instance;

	/**
	 * 
	 */
	public SimulatedAnnealing(Instance instance) {
		super();
		this.instance = instance;
	}

	/**
	 * @param temperature
	 * @param coolingRate
	 */
	public SimulatedAnnealing(double temperature, double coolingRate) {
		super();
		this.temperature = temperature;
		this.coolingRate = coolingRate;
	}

	/**
	 * @return the temperature
	 */
	public double getTemperature() {
		return temperature;
	}

	/**
	 * @return the coolingRate
	 */
	public double getCoolingRate() {
		return coolingRate;
	}

	/**
	 * @param temperature
	 *            the temperature to set
	 */
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	/**
	 * @param coolingRate
	 *            the coolingRate to set
	 */
	public void setCoolingRate(double coolingRate) {
		this.coolingRate = coolingRate;
	}

	public static double calculateAcceptanceProbability(int energy,
			int newEnergy, double temperature) {
		// If the new solution is better, accept it
		if (newEnergy < energy) {
			return 1.0;
		}
		// If the new solution is worse, calculate an acceptance probability
		return Math.exp((energy - newEnergy) / temperature);
	}

	public double calculateInitialTemperature(int neighborhood) {
		double temperature = 0;
		Phylogeny phylogeny = new Phylogeny();
		List<int[]> population = phylogeny.generatePopulation(neighborhood,
				instance.getNumberOfSpecies());
		int totalCost = 0;
		int elementCost = 0;
		int quantity = 0;
		for (int[] element : population) {
			elementCost = phylogeny.cost(instance.getNumberOfSpecies(),
					instance.getNumberOfCharacter(), element,
					instance.getMatrix());
			instance.getSolution().update(elementCost);
			if (elementCost < instance.getSolutionFinded()
					|| instance.getSolutionFinded() == 0) {
				instance.setSolutionFinded(elementCost);
				instance.setSolutionTree(element);
			}
		}
		for (int[] element : population) {
			elementCost = phylogeny.cost(instance.getNumberOfSpecies(),
					instance.getNumberOfCharacter(), element,
					instance.getMatrix());
			if (elementCost > instance.getSolutionFinded()) {
				totalCost += (elementCost - instance.getSolutionFinded());
				quantity++;
			}
		}

		temperature = (totalCost / quantity) * 4.48;
		setTemperature(temperature);
		return temperature;
	}

	public HashMap<String, int[]> findBestNeighbor(int[] tree, int cost) {
		Phylogeny phylogeny = new Phylogeny();
		int[] neighbor = tree.clone();
		int[] bestNeighbor = tree.clone();
		int costNeighbor = cost;
		int costBestNeighbor = cost;
		HashMap<String, int[]> map = new HashMap<String, int[]>(2);

		for (int i = 0; i < tree.length - 2; i++) {
			for (int j = i + 1; j < tree.length - 1; j++) {
				if (tree[i] != tree[j] && tree[i] > j) {
					neighbor = phylogeny.permute(tree, i, j);
					if (phylogeny.isViable(instance.getNumberOfSpecies(),
							tree.length, neighbor)) {
						costNeighbor = phylogeny.costMin(
								instance.getNumberOfSpecies(),
								instance.getNumberOfCharacter(), neighbor,
								instance.getMatrix(), costBestNeighbor);
						if (costNeighbor > 0 & costNeighbor < costBestNeighbor) {
							bestNeighbor = neighbor.clone();
							costBestNeighbor = costNeighbor;
						}
					} else {
						neighbor = phylogeny.permute(neighbor, j, i);
					}
				}
			}
		}
		if (costBestNeighbor == cost) {
			int position1 = MathUtil.randomWithRange(0,
					instance.getNumberOfSpecies() - 1);
			for (int position2 = position1 + 1; position2 < tree.length - 2; position2++) {
				if (position1 != position2) {
					for (int position3 = position2 + 1; position3 < tree.length - 1; position3++) {
						if (tree[position2] != tree[position3]
								&& tree[position2] > position3) {
							if (position1 != position2
									&& position1 != position3) {
								for (int w = 1; w <= 5; w++) {
									neighbor = phylogeny.permute3(bestNeighbor,
											w, position1, position2, position3);
									if (phylogeny.isViable(
											instance.getNumberOfSpecies(),
											tree.length, neighbor)) {
										costNeighbor = phylogeny
												.costMin(
														instance.getNumberOfSpecies(),
														instance.getNumberOfCharacter(),
														neighbor, instance
																.getMatrix(),
														costBestNeighbor);
										if (costNeighbor > 0
												& costNeighbor < costBestNeighbor) {
											bestNeighbor = neighbor.clone();
											costBestNeighbor = costNeighbor;
										}
									}
								}
							}
						}
					}
				}

			}

		}

		map.put("cost", new int[] { costBestNeighbor });
		map.put("tree", bestNeighbor);
		return map;
	}

	public void anneal() {
		setTemperature(calculateInitialTemperature(instance
				.getNumberOfSpecies() * 2));
		int S0 = instance.getSolutionFinded();
		int S = S0;
		int[] solutionTree = instance.getSolutionTree().clone();
		instance.getSolution().setInitial(S0);
		instance.getSolution().update(S0);
		int nIter = (int) (Math.log(0.001 / temperature) / Math
				.log(coolingRate));
		System.out.println(instance.getName() + " | alpha = "+ coolingRate + " | Temp. Inicial = " + temperature + " | NITER = " + nIter);
		while (temperature > 0.001) {
			int count = 0;
			do {
				count++;
				HashMap<String, int[]> map = findBestNeighbor(solutionTree, S);
				solutionTree = map.get("tree");
				S = map.get("cost")[0];
				instance.getSolution().update(S);
				if (calculateAcceptanceProbability(S0, S, temperature) > Math
						.random()) {
					S0 = S;
					if (S0 < instance.getSolutionFinded()) {
						instance.setSolutionFinded(S);
						instance.setSolutionTree(solutionTree.clone());
					}
				}
			} while (count < nIter);
			temperature *= (coolingRate);
		}
	}

	public static void main(String args[]) {

		FileHandler handler = new FileHandler("input", "output");
		List<Instance> instances = handler.readInstancesFromFile();
		SimulatedAnnealing sa;
		for (Instance instance : instances) {
			OutputSolution solution = new OutputSolution();
			instance.setSolution(solution);
			sa = new SimulatedAnnealing(instance);
			sa.setCoolingRate(0.5);
			long startTime = System.currentTimeMillis();
			for (int i = 0; i < 1; i++) {
				sa.anneal();
			}
			long endTime = System.currentTimeMillis();
			solution.setTime(endTime - startTime);
			double gap = instance.getBestSolution() > 0 ? (double) instance
					.getSolutionFinded() / (double) instance.getBestSolution()
					: 0;
			solution.setGap(gap);
			solution.setBest(instance.getSolutionFinded());
			System.out.println(instance.getName() + " | m = "
					+ instance.getNumberOfSpecies() + " | n = "
					+ instance.getNumberOfCharacter() + " | �timo = "
					+ instance.getBestSolution() + " | inicial = "
					+ solution.getInitial() + " | m�ximo = "
					+ solution.getMaximum() + " | m�dia = "
					+ solution.getAverage() + " | solu��o = "
					+ instance.getSolutionFinded() + " | gap = " + gap
					+ " | tempo = " + solution.getTimeString());
			System.out.println();
		}
	}
}
