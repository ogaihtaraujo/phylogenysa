/**
 * 
 */
package br.uece.macc.paa.eqb.model;

/**
 * @author thiago
 *
 */
public class Instance {
	
	private String name;
	private int numberOfSpecies;
	private int numberOfCharacter;
	private int bestSolution;
	
	private int matrix[][];
	
	private String species[];
	
	private int solutionFinded;
	private int[] solutionTree;
	
	private OutputSolution solution;

	/**
	 * 
	 */
	public Instance() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param numberOfSpecies
	 * @param numberOfCharacter
	 * @param bestSolution
	 */
	public Instance(String name, int numberOfSpecies, int numberOfCharacter, int bestSolution) {
		super();
		this.name = name;
		this.numberOfSpecies = numberOfSpecies;
		this.numberOfCharacter = numberOfCharacter;
		this.bestSolution = bestSolution;
		this.matrix = new int[2*numberOfSpecies-1][numberOfCharacter];
		this.species = new String[numberOfSpecies];
	}

	/**
	 * @return the numberOfSpecies
	 */
	public int getNumberOfSpecies() {
		return numberOfSpecies;
	}

	/**
	 * @return the numberOfCharacter
	 */
	public int getNumberOfCharacter() {
		return numberOfCharacter;
	}

	/**
	 * @return the bestSolution
	 */
	public int getBestSolution() {
		return bestSolution;
	}

	/**
	 * @return the matrix
	 */
	public int[][] getMatrix() {
		return matrix;
	}

	/**
	 * @return the solution
	 */
	public OutputSolution getSolution() {
		return solution;
	}

	/**
	 * @param numberOfSpecies the numberOfSpecies to set
	 */
	public void setNumberOfSpecies(int numberOfSpecies) {
		this.numberOfSpecies = numberOfSpecies;
	}

	/**
	 * @param numberOfCharacter the numberOfCharacter to set
	 */
	public void setNumberOfCharacter(int numberOfCharacter) {
		this.numberOfCharacter = numberOfCharacter;
	}

	/**
	 * @param bestSolution the bestSolution to set
	 */
	public void setBestSolution(int bestSolution) {
		this.bestSolution = bestSolution;
	}

	/**
	 * @param matrix the matrix to set
	 */
	public void setMatrix(int[][] matrix) {
		this.matrix = matrix;
	}

	/**
	 * @param solution the solution to set
	 */
	public void setSolution(OutputSolution solution) {
		this.solution = solution;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the species
	 */
	public String[] getSpecies() {
		return species;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param species the species to set
	 */
	public void setSpecies(String[] species) {
		this.species = species;
	}

	/**
	 * @return the solutionFinded
	 */
	public int getSolutionFinded() {
		return solutionFinded;
	}

	/**
	 * @return the solutionTree
	 */
	public int[] getSolutionTree() {
		return solutionTree;
	}

	/**
	 * @param solutionFinded the solutionFinded to set
	 */
	public void setSolutionFinded(int solutionFinded) {
		this.solutionFinded = solutionFinded;
	}

	/**
	 * @param solutionTree the solutionTree to set
	 */
	public void setSolutionTree(int[] solutionTree) {
		this.solutionTree = solutionTree;
	}
	
}
