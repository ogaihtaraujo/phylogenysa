/**
 * 
 */
package br.uece.macc.paa.eqb.model;

import java.sql.Time;

/**
 * @author thiago
 *
 */
public class OutputSolution {
	
	private int initial;
	private int maximum;
	private int best;
	private double average;
	private int accumulator;
	private int amount;
	private long time;
	private double gap;
	

	/**
	 * 
	 */
	public OutputSolution() {
		// TODO Auto-generated constructor stub
	}


	/**
	 * @param initial
	 * @param maximum
	 * @param best
	 * @param average
	 * @param time
	 * @param gap
	 */
	public OutputSolution(int initial, int maximum, int best, double average,
			long time, double gap) {
		super();
		this.initial = initial;
		this.maximum = maximum;
		this.best = best;
		this.average = average;
		this.time = time;
		this.gap = gap;
	}


	/**
	 * @return the initial
	 */
	public int getInitial() {
		return initial;
	}


	/**
	 * @return the maximum
	 */
	public int getMaximum() {
		return maximum;
	}


	/**
	 * @return the best
	 */
	public int getBest() {
		return best;
	}


	/**
	 * @return the average
	 */
	public double getAverage() {
		average = accumulator/amount;
		return average;
	}


	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}

	/**
	 * @return the time
	 */
	public String getTimeString() {
		long second = (this.time / 1000) % 60;
		long minute = (this.time / (1000 * 60)) % 60;
		long hour = (this.time / (1000 * 60 * 60)) % 24;

		String time = String.format("%02d:%02d:%02d:%d", hour, minute,
				second, this.time);
		return time;
	}
	

	/**
	 * @return the gap
	 */
	public double getGap() {
		return gap;
	}


	/**
	 * @param initial the initial to set
	 */
	public void setInitial(int initial) {
		this.initial = initial;
	}


	/**
	 * @param maximum the maximum to set
	 */
	public void setMaximum(int maximum) {
		this.maximum = maximum;
	}


	/**
	 * @param best the best to set
	 */
	public void setBest(int best) {
		this.best = best;
	}


	/**
	 * @param average the average to set
	 */
	public void setAverage(double average) {
		this.average = average;
	}


	/**
	 * @param time the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}


	/**
	 * @param gap the gap to set
	 */
	public void setGap(double gap) {
		this.gap = gap;
	}


	/**
	 * @return the accumulator
	 */
	public int getAccumulator() {
		return accumulator;
	}


	/**
	 * @return the amount
	 */
	public int getAmount() {
		return amount;
	}


	/**
	 * @param value the value to increase
	 */
	public void increaseAccumulator(int value) {
		this.accumulator += value;
	}


	/**
	 * @param amount the amount to set
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}

	public void update(int cost){
		accumulator += cost;
		amount++;
		if(cost > maximum){
			maximum = cost;
		}
	}
	
}
