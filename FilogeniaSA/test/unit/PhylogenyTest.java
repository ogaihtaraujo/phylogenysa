/**
 * 
 */
package unit;

import static org.junit.Assert.fail;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import br.uece.macc.paa.eqb.controller.Phylogeny;
import br.uece.macc.paa.eqb.model.Instance;

/**
 * @author thiago
 * 
 */
public class PhylogenyTest {

	private static Instance instance;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		instance = new Instance("teste", 4, 6, 0);
		int M[][] = instance.getMatrix();
		M[0][0] = 0;
		M[0][1] = 2;
		M[0][2] = 0;
		M[0][3] = 2;
		M[0][4] = 0;
		M[0][5] = 2;

		M[1][0] = 0;
		M[1][1] = 1;
		M[1][2] = 1;
		M[1][3] = 0;
		M[1][4] = 1;
		M[1][5] = 1;

		M[2][0] = 0;
		M[2][1] = 0;
		M[2][2] = 1;
		M[2][3] = 1;
		M[2][4] = 1;
		M[2][5] = 0;

		M[3][0] = 1;
		M[3][1] = 0;
		M[3][2] = 0;
		M[3][3] = 1;
		M[3][4] = 1;
		M[3][5] = 0;
	}

	/**
	 * Test method for
	 * {@link br.uece.macc.paa.eqb.controller.Phylogeny#isViable(int, int, int[])}
	 * .
	 */
	@Test
	public void testIsUnviable() {

		Phylogeny phylogeny = new Phylogeny();
		int numberOfSpecies = instance.getNumberOfSpecies();
		int tree[] = new int[2 * numberOfSpecies - 1];
		int start = numberOfSpecies;
		for (int i = 0; i < tree.length; i++) {
			tree[i] = start;
			if (i > 0 && i % 2 == 0) {
				start++;
			}
		}
		boolean result = phylogeny.isViable(instance.getNumberOfSpecies(),
				tree.length, tree);
		Assert.assertFalse("invalid", result);
	}

	/**
	 * Test method for
	 * {@link br.uece.macc.paa.eqb.controller.Phylogeny#isViable(int, int, int[])}
	 * .
	 */
	@Test
	public void testIsViable() {

		Phylogeny phylogeny = new Phylogeny();
		int numberOfSpecies = 4;
		 int tree[] = { 6, 5, 4, 4, 5, 6, 7 };
		// int tree[] = { 4, 4, 5, 5, 6, 6, 7 };
		boolean result = phylogeny.isViable(numberOfSpecies, tree.length, tree);
		Assert.assertTrue("valid", result);
	}

	/**
	 * Test method for
	 * {@link br.uece.macc.paa.eqb.controller.Phylogeny#costNode(int, int[], int[])}
	 * .
	 */
	@Test
	public void testCostNode() {
		Phylogeny phylogeny = new Phylogeny();
		int numberOfCharacter = 6;
		int[] leftChild = { 1, 0, 0, 1, 1, 0 };
		int[] rightChild = { 0, 0, 1, 1, 1, 0 };
		int cost = 0;
		HashMap<String, int[]> map = phylogeny.costNode(numberOfCharacter,
				leftChild, rightChild);

		int[] father = map.get("father");
		cost += map.get("cost")[0];

		Assert.assertArrayEquals(new int[] { 2, 0, 2, 1, 1, 0 }, father);
		Assert.assertEquals(2, cost);
	}

	/**
	 * Test method for
	 * {@link br.uece.macc.paa.eqb.controller.Phylogeny#cost(int, int, int[], int, int[][])}
	 * .
	 */
	@Test
	public void testCost() {
		Phylogeny phylogeny = new Phylogeny();
		int[][] BigMatrix = new int[2 * instance.getNumberOfSpecies() - 1][instance
				.getNumberOfCharacter()];
		int cost = 0;
		int upperBound = 20;
//		int tree[] = { 6, 5, 4, 4, 5, 6, 7 };
		int tree[] = {4, 5, 6, 4, 7, 5, 6};
		for (int i = 0; i < instance.getNumberOfSpecies(); i++) {
			for (int j = 0; j < instance.getNumberOfCharacter(); j++) {
				BigMatrix[i][j] = instance.getMatrix()[i][j];
			}
		}

		cost = phylogeny.cost(instance.getNumberOfSpecies(),
				instance.getNumberOfCharacter(), tree, BigMatrix);

		Assert.assertEquals(7, cost);

	}

	/**
	 * Test method for
	 * {@link br.uece.macc.paa.eqb.controller.Phylogeny#cost(int, int, int[], int, int[][])}
	 * .
	 */
	/*	@Test
	public void testUpperBoundCost() {
		Phylogeny phylogeny = new Phylogeny();
		int[][] BigMatrix = new int[2 * instance.getNumberOfSpecies() - 1][instance
				.getNumberOfCharacter()];
		int cost = 0;
		int upperBound = 3;
		int tree[] = { 6, 5, 4, 4, 5, 6, 7 };

		for (int i = 0; i < instance.getNumberOfSpecies(); i++) {
			for (int j = 0; j < instance.getNumberOfCharacter(); j++) {
				BigMatrix[i][j] = instance.getMatrix()[i][j];
			}
		}

		cost = phylogeny.cost(instance.getNumberOfSpecies(),
				instance.getNumberOfCharacter(), tree, upperBound, BigMatrix);

		Assert.assertEquals(-1, cost);

	}*/
}
